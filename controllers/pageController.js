const  dbconnect = require('../resource/callsql');
exports.index = async (req, res, next) => {
    try {
        res.render('./pages/login.ejs',{ title:'PDA Login' ,navbar:'none'});
        // if (existEmail) {
        //     const error = new Error('อีเมล์ซ้ำ มีผู้ใช้งานแล้ว ลองใหม่อีกครั้ง');
        //     error.statusCode = 400;
        //     throw error;
        // }
    } catch (error) {
        next(error); 
    }
}

exports.changepassword = async (req, res, next) => {
    try {
        res.render('./pages/changePassword.ejs',{ title:'PDA Change Password'});
    } catch (error) {
        next(error); 
    }
}

exports.maincontrol = async (req, res, next) => {
    try {
        let stCode = req.cookies['CCSTCODE'] ;
        let stFCode = req.cookies['CCSTFCODE'];
        let stSCode = req.cookies['CCSTSCODE'];
        let stName = req.cookies['CCSTNAME'];
        let cntNum = req.cookies['CCCNTNUM'];
        let userName = req.user.CCUSERID;
        let qry =`exec CCTSQL..PDACC_GetCntMain '${cntNum}'`;
        let getsql = await dbconnect.getquery(qry);
        let cntItem = getsql.dbitems[0];
        res.render('./pages/maincontrol.ejs',{ title:'PDA Main Control',  
            stCode: stCode, 
            stFCode: stFCode, 
            stName: stName,
            userName : userName,
            cntItem : cntItem,
            cntNum : cntNum,
            stSCode: stSCode
         });
    } catch (error) {
        next(error); 
    }
}

exports.scanonly = async (req, res, next) => {
    try {
        let stCode = req.cookies['CCSTCODE'] ;
        let stFCode = req.cookies['CCSTFCODE'];
        let stName = req.cookies['CCSTNAME'];
        let cntNum = req.cookies['CCCNTNUM'];
        let userName = req.user.CCUSERID;
        let qry =`exec CCTSQL..PDACC_GetCntMain '${cntNum}'`;
        let getsql = await dbconnect.getquery(qry);
        let cntItem = getsql.dbitems[0];
        res.render('./pages/scanonly.ejs',{ title:'PDA Scan Only', 
            stCode: stCode, 
            stFCode: stFCode, 
            stName: stName,
            userName : userName,
            cntItem : cntItem,
            cntNum : cntNum,
            headerName : "สแกนนับ 1"
         });
    } catch (error) {
        next(error); 
    }
}

exports.scannumber = async (req, res, next) => {
    try {
        let stCode = req.cookies['CCSTCODE'] ;
        let stFCode = req.cookies['CCSTFCODE'] ;
        let stName = req.cookies['CCSTNAME'] ;
        let cntNum = req.cookies['CCCNTNUM'] ;
        let userName = req.user.CCUSERID ;
        let qry =`exec CCTSQL..PDACC_GetCntMain '${cntNum}'`;
        let getsql = await dbconnect.getquery(qry);
        let cntItem = getsql.dbitems[0];
        res.render('./pages/scannumber.ejs',{ title:'PDA Scan With Number', 
            stCode: stCode, 
            stFCode: stFCode, 
            stName: stName,
            userName : userName,
            cntItem : cntItem,
            cntNum : cntNum,
            headerName : "สแกนใส่จำนวน"
         });
    } catch (error) {
        next(error); 
    }
}

exports.scannewproduct = async (req, res, next) => {
    try {
        let stCode = req.cookies['CCSTCODE'] ;
        let stFCode = req.cookies['CCSTFCODE'] ;
        let stName = req.cookies['CCSTNAME'] ;
        let cntNum = req.cookies['CCCNTNUM'] ;
        let userName = req.user.CCUSERID ;
        let qry =`exec CCTSQL..PDACC_GetCntMain '${cntNum}'`;
        let getsql = await dbconnect.getquery(qry);
        let cntItem = getsql.dbitems[0];
        res.render('./pages/scannewproduct.ejs',{ title:'PDA Scan New Product', 
            stCode: stCode, 
            stFCode: stFCode, 
            stName: stName,
            userName : userName,
            cntItem : cntItem,
            cntNum : cntNum,
            headerName : "สแกนเพิ่มสินค้าใหม่"
         });
    } catch (error) {
        next(error); 
    }
}

exports.scanwithnumber = async (req, res, next) => {
    try {
        // const profileList = req.cookies.CCUPROFILE;
        res.render('./pages/scanwithnumber.ejs',{ title:'PDA scannumber Control'});
    } catch (error) {
        next(error); 
    }
}

exports.editvariance = async (req, res, next) => {
    try {
        let stCode = req.cookies['CCSTCODE'] ;
        let stFCode = req.cookies['CCSTFCODE'] ;
        let stName = req.cookies['CCSTNAME'] ;
        let cntNum = req.cookies['CCCNTNUM'] ;
        let userName = req.user.CCUSERID ;
        let qry =`exec CCTSQL..PDACC_GetCntMain '${cntNum}'`;
        let getsql = await dbconnect.getquery(qry);
        let cntItem = getsql.dbitems[0];
        res.render('./pages/editvariance.ejs',{ title:'PDA Edit Variance', 
            stCode: stCode, 
            stFCode: stFCode, 
            stName: stName,
            userName : userName,
            cntItem : cntItem,
            cntNum : cntNum,
            headerName : "แก้ไข variance"
         });
    } catch (error) {
        next(error); 
    }
}

exports.selectstore = async (req, res, next) => {
    try {
       
        res.render('./pages/selectstore.ejs',{ 
            title:'PDA scannumber editvariance',
            headerName : "เปลี่ยนใบนับ",
            navbar:'none'
        });
    } catch (error) {
        next(error); 
    }
}
exports.edititemcount = async (req, res, next) => {
    try {
        let stCode = req.cookies['CCSTCODE'] ;
        let stFCode = req.cookies['CCSTFCODE'] ;
        let stName = req.cookies['CCSTNAME'] ;
        let cntNum = req.cookies['CCCNTNUM'] ;
        let userName = req.user.CCUSERID ;
        let qry =`exec CCTSQL..PDACC_GetCntMain '${cntNum}'`;
        let getsql = await dbconnect.getquery(qry);
        let cntItem = getsql.dbitems[0];
        res.render('./pages/edititemcount.ejs',{ title:'PDA Edit Itemcount', 
            stCode: stCode, 
            stFCode: stFCode, 
            stName: stName,
            userName : userName,
            cntItem : cntItem,
            cntNum : cntNum,
            headerName : "รายการนับแล้ว"
         });
    } catch (error) {
        next(error); 
    }
}

exports.listitemnotcount = async (req, res, next) => {
    try {
        let stCode = req.cookies['CCSTCODE'] ;
        let stFCode = req.cookies['CCSTFCODE'] ;
        let stName = req.cookies['CCSTNAME'] ;
        let cntNum = req.cookies['CCCNTNUM'] ;
        let userName = req.user.CCUSERID ;
        let qry =`exec CCTSQL..PDACC_GetCntMain '${cntNum}'`;
        let getsql = await dbconnect.getquery(qry);
        let cntItem = getsql.dbitems[0];
        res.render('./pages/listitemnotcount.ejs',{ title:'PDA List Item Not Count', 
            stCode: stCode, 
            stFCode: stFCode, 
            stName: stName,
            userName : userName,
            cntItem : cntItem,
            cntNum : cntNum,
            headerName : "รายการยังไม่นับ"
         });
    } catch (error) {
        next(error); 
    }
}