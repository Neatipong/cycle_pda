function showselecttable(_this){
    let getclass = $(".scanlist");
        for(let i=0; i<getclass.length; i++){
            let element = getclass.eq(i);
            $(element).removeClass("active");
        }
        $(_this).addClass( "active" );
    }
    async function getSKUScanLists() {
        try {
                let cntNum = readCookie('CCCNTNUM');
                let userName = parseJwt(readCookie('CCTOKEN')).CCUSERID
                let item = {
                    CNTNUM: cntNum,
                    USERID: userName,
                    LOCATION: null,
                    SKUBAR: null
                }

                let getapi = await call_api("POST", "../api/listedit", item, 'auth');
                if (getapi.data.dbrows == 0) {
                let tableBody = `<p class="text-center mt-2">ไม่พบข้อมูล</p>`;
                document.getElementById("skuScanLists").innerHTML = tableBody;
                } else {
                    let tableBody = `
                    <div class="view">
                        <div class="wrapper">
                            <table class="table">
                            <thead>
                                <tr>
                                <th class="sticky-col text-center first-col">Location</th>
                                <th class="sticky-col text-center second-col">SKU</th>
                                <th class="sticky-col text-center three-col">จำนวน</th>
                                <th>Barcode</th>
                                <th>ชื่อสินค้า</th>
                                <th>ยี่ห้อ</th>
                                
                                </tr>
                            </thead>
                            <tbody>
                                ${getapi.data.dbitems.map((item, i) => `
                                <tr class="scanlist " onclick="showselecttable(this)" >
                                    <td class="sticky-col text-center first-col">${item.LOCATION}</td>
                                    <td class="sticky-col text-center second-col">${item.SKU}</td>
                                    <td class="sticky-col text-center three-col">${item.CNTQNT}</td>
                                    <td>${item.BARCODE}</td>
                                    <td>${item.PRNAME}</td>
                                    <td>${item.BRAND}</td>
                                </tr>
                                `).join("")}
                     
                            </tbody>
                            </table>
                        </div>
                    </div>

                    `;
                    document.getElementById("skuScanLists").innerHTML = tableBody;
                }
        } catch (error) {
            alertMessage('showerr', error, 10000);
        }
    }
    async function addlocation() {
        try {
            let location = document.getElementById("Location").value;
            let cntNum = readCookie('CCCNTNUM');
            let userName = parseJwt(readCookie('CCTOKEN')).CCUSERID;
            let item = {
                CNTNUM: cntNum,
                LOCATION: location,
                USERID: userName,
            }
            let getapi = await call_api("POST", "../api/addcntlocation", item, 'auth');
            let jsn = getapi.data;
            if (!(jsn.dbcode == 0)) {
                alertMessage('showerr', jsn.dbmessage, 10000);
                setValue('Location', '');
                setFocus('Location');
                return;
            }

            let result = jsn.dbitems[0].RESULT;
            if (result != 'OKOK') {
                alertMessage('showwarning', 'ไม่สามารถ Add Location ได้ ' + result, 10000);
                setValue('Location', '');
                setFocus('Location');
                return;
            }
            alertMessage('showsuccess', `Add Location ${location} สำเร็จ`, 10000);
            document.getElementById("Location").setAttribute('disabled', true);
            setFocus('numberSKU');
        } catch (error) {
            alertMessage('showerr', error, 10000);
        }
    }
    async function setLocation(_this) {
        try {   
          
            if (_this.keyCode == 13) {
                let location = document.getElementById("Location").value;
                let cntNum = readCookie('CCCNTNUM');
                if (IsNullOrWhiteSpace(location)) {
                    alertMessage('showwarning', 'กรุณาระบุ Location ให้ถูกต้อง', 10000);
                    setValue('Location', '');
                    setFocus('Location');
                    return;
                }
                let item = {
                    CNTNUM: cntNum,
                    LOCATION: location
                }
                let getapi = await call_api("POST", "../api/checklocation", item, 'auth');
                let jsn = getapi.data;
                if (!(jsn.dbcode == 0)) {
                    swalConfirm('error', 'Failed', jsn.dbmessage);
                    alertMessage('showerr', jsn.dbmessage, 10000);
                    setValue('Location', '');
                    setFocus('Location');
                    return;
                }

                let result = jsn.dbitems[0].RESULT;
                if (!result.includes('OKAD')) {
                    if (result != 'OKOK') {
                        alertMessage('showwarning', `ไม่สามารถเลือก Location ${location} นี้ได้`, 10000);
                        setValue('Location', '');
                        setFocus('Location');
                        return;
                    }
                    document.getElementById("Location").setAttribute('disabled', true);
                    $( "#numberSKU" ).prop( "disabled", false );

                    setFocus('numberSKU');
                    return;
                }
                swal({
                        title: `ไม่มี Location นี้ในใบนับ`,
                        text: `คุณต้องการเพิ่ม ${location} หรือไม่`,
                        type: null,
                        showCancelButton: true,
                        confirmButtonText: "ยกเลิก",
                        confirmButtonClass: 'btn-danger',
                        cancelButtonText: "ตกลง",
                        cancelButtonClass: 'btn-primary',
                        closeOnCancel: true,
                    },
                    function (isCancel) {
                        if (isCancel) {
                            document.getElementById("Location").removeAttribute('disabled');
                            document.getElementById("numberSKU").setAttribute('disabled', true);
                            setValue('Location', '');
                            setFocus('Location');
                            return;
                        };
                        $( "#numberSKU" ).prop( "disabled", false );
                        $(".cancel").attr('disabled', true);
                        addlocation();
                    }
                );
            }
        } catch (error) {
            alert(error)
        }

    }
    function setResultScanSKU(itms,para_SKU) {
        let {
            BRAND,
            COLOR,
            MODEL,
            MULTISALE,
            PRNAME,
            PRTYPE,
            SIZE,
            UNITSIZE,
            SKCODE,
            IMAGE,
            RTVWORK,
            ALCTOOUT,
            SKUCOLOR
        } = itms;
        let colorArr = SKUCOLOR ? JSON.parse(SKUCOLOR) : [];
        let colorHtml = ``,imageHtml = ``;
        for(let i = 0; i < colorArr.length; i++) {
            colorHtml += `<span class='flex-item' style="margin: 2px 5px; padding: 10px 40px; background-color: ${colorArr[i]}; border-radius: 4px; border-color: red; border-style: solid; border-color: white; "></span>`
        }
        colorHtml = `<div class='flex-container'>${colorHtml}</div>`
        imageHtml = IMAGE ? `<center>
                                <img src="data:image/jpeg;base64,${IMAGE}" width="50%" alt="">
                            </center><br>`
                        : '';
        let html = `${imageHtml + colorHtml}
        <hr style="border-top: 1px solid #fff;">
                    <div class="row">
                        <div class="col-12">
                            <center><p class="m-0 p-0"> <span >ข้อมูล SKU:</span> ${SKCODE}</p></center>
                        </div>
                        <div class="col-12 mt-1">
                            <div class="row boxwhite">
                                <div class="col-12">
                                    <p class='scan-txt'> <span class="pbstyle">ชื่อสินค้า:</span> ${PRNAME}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt-1">
                            <div class="row boxwhite">
                                <div class="col-6">
                                    <p class='scan-txt'> <span class="pbstyle">ยี่ห้อ:</span> ${BRAND}</p>
                                </div>
                                <div class="col-6">
                                    <p class='scan-txt'> <span class="pbstyle">รุ่น:</span> ${MODEL}</p>
                                </div>
                                <div class="col-6">
                                    <p class='scan-txt'> <span class="pbstyle">สี:</span> ${COLOR}</p>
                                </div>
                                <div class="col-6">
                                    <p class='scan-txt'> <span class="pbstyle">Multisale:</span> ${MULTISALE}</p>
                                </div>
                                <div class="col-6">
                                    <p class='scan-txt'> <span class="pbstyle">ไซส์:</span> ${SIZE}</p>
                                </div>
                                <div class="col-6">
                                    <p class='scan-txt'> <span class="pbstyle">หน่วย:</span> ${UNITSIZE}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt-1">
                            <div class="row boxwhite">
                                <div class="col-6">
                                    <p class='scan-txt'> <span class="pbstyle">RTV:</span> ${RTVWORK} ${(RTVWORK=='0')?`<a onclick="GetRTV('${para_SKU}')" class="RTV_BTN">INST</a>`:''}</p>
                                </div>
                                <div class="col-6">
                                    <p class='scan-txt'> <span class="pbstyle">TRF:</span> ${ALCTOOUT}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                `;
        document.getElementById("resultSKU").innerHTML = html +
            `<hr style="border-top: 1px solid #fff;">`;
    }
    async function GetRTV(para_SKU) {
        let getSTCODE = readCookie('CCSTCODE');
        let item = {
            SKU: '60083612',
            STCODE: '934'
        }

        let getapi = await call_api("POST", "../api/getrtv", item, 'auth');
        console.log("🚀 ~ file: mainjs.js ~ line 243 ~ GetRTV ~ getapi", getapi)
        let getData = getapi.data.dbitems;
        // dbitems: Array(1)
        // 0:
        // จำนวนตั้งต้น: 30
        // วันที่สร้าง Instruction: null
        // วันที่อนุมัติ: "2017-06-10T15:45:27.893Z"
        // ใบแจ้งขอส่งคืน: "R170500216"
        // __proto__: Object
        // length: 1
        // __proto__: Array(0)
        
        if (getData.length == 0) {
            alertMessage('showerr', "ไม่พบข้อมูล", 10000);
        } else {
            $('#ModalRTV').modal('show');
            let rtvHtml = '';
            getData.forEach((itm, idx, arr) => {
                rtvHtml += `<div class='row'>
                                <div class="col-12">
                                    <p class='rtv-txt'> <span class="pbstyle">ใบแจ้งขอส่งคืน:</span> <span>${itm["ใบแจ้งขอส่งคืน"]}</span> </p>
                                </div>
                                <div class="col-12">
                                    <p class='rtv-txt'> <span class="pbstyle">วันที่อนุมัติ:</span> <span>${itm["วันที่อนุมัติ"]}</span> </p>
                                </div>
                                <div class="col-12">
                                    <p class='rtv-txt'> <span class="pbstyle">วันที่สร้าง Instruction:</span> <span>${itm["วันที่สร้าง Instruction"]}</span> </p>
                                </div>
                                <div class="col-12">
                                    <p class='rtv-txt'> <span class="pbstyle">จำนวนตั้งต้น:</span> <span>${itm["จำนวนตั้งต้น"]}</span> </p>
                                </div>
                                <div class="col-12">
                                    <hr style="border-top: 1px solid #aaa;">
                                </div>
                            </div>`
                if(idx == arr.length - 1) {
                    $('#rtvDiv').html(rtvHtml)
                }
            })
            // $('#RTVcomeback').text(getData["ใบแจ้งขอส่งคืน"])
            // $('#RTVapprove').text(getData["วันที่อนุมัติ"])
            // $('#RTVcreate').text(getData["วันที่สร้าง Instruction"])
            // $('#RTVstart').text(getData["จำนวนตั้งต้น"])
        }
    }

    function changeLocation(obj) {
        let location = document.getElementById("Location").value;
        swal({
                title: ``,
                text: `คุณต้องการเปลี่ยน Location ${location} นี้หรือไม่`,
                type: null,
                showCancelButton: true,
                confirmButtonText: "ยกเลิก",
                confirmButtonClass: 'btn-danger',
                cancelButtonText: "ตกลง",
                cancelButtonClass: 'btn-primary',
                closeOnCancel: true,
            },
            function (isCancel) {
                if (isCancel) {
                    //ยกเลิก
                    return;
                }else{
                    document.getElementById("scanonly").reset()
                    document.getElementById("Location").removeAttribute('disabled');
                    $( "#numberSKU" ).prop( "disabled", true );
                    document.getElementById("resultSKU").innerHTML = ``;
                    clearMessageBox();
                    setFocus("Location");
                }
            }
        );
    }
        