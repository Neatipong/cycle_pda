var express 	= require('express')
 
var router 		= express.Router()
var dt 			= require('../resource/datetime') 
var request		=require('request')
var db			=require('../resource/dbconnect') 
const sharp = require('sharp');

// https://sharp.pixelplumbing.com/en/stable/api-input/

router.use(function timeLog (req, res, next) {
	  console.log('call Image Router  Time: ', dt.myDateTime())
	  next()
	})
	
router.post('/TODB' , function (req, res) {
 
	var imgurl  =req.body.url;
	var skc = req.body.skc;
	var lupd = req.body.lupd;
	var enrich = req.body.enrich;
 
	request({url: imgurl, encoding: null}, 
			function(err, result, bodyBuffer) {
				if(err){
					console.log('Image2DB .'+err);
					res.end('Image2DB ERR Cant get Image from '+imgurl)
				}
				else {
					sharp(bodyBuffer)
					.resize(500,500)
			    	.toBuffer()
			    	.then( data=>{
			    			var buf =data.toString('base64');
			    			var cmd="if not exists(select skc from twdpim..pimImage where SKC=@skcode)  " +
							"Insert twdpim..PimImage(SKC,BASE64,LastUpdatePIM,UpdateDate, Enrich) values('@skcode','@base','@lupd', getdate(), '@enrich') " +
							"else Update twdpim..Pimimage Set BASE64='@base', LastUpdatePIM='@lupd', UpdateDate=getdate(), Enrich='@enrich' where SKC=@skcode";
			    				 var pdata={
			    						 xid:0,
			    						 qry:cmd,
			    						 par:{
			    							 skcode:skc,
											 base:buf,
											 lupd: lupd,
											 enrich: enrich
			    						    }
			    				 	       }
			    				  db.execute(pdata,function(resp){  //-----return with req ID
			     					   if(resp.dbcode==0){
			     						  res.send('image2DB Copy & Resize Image Successful')
			     					   }
			     					   else
			     						  res.send('ERR '+resp.dbmessage);
			    				 });
			    				 
			    			   })
			    			   
			    	.catch( err => {
			    		     console.log('Sharp Error '+err);
			    		     res.send('image2DB ERR '+err)
			    			} 
			    	      );
					
				}

		});

});
 
router.post('/FILETODB' , function (req, res) {
 
	//var fname  =req.body.fname;  //'./public/uploads/userPhoto.jpg'; // req.body.fname;
	var fname  ='./public/uploads/userPhoto.jpg'; // req.body.fname;
	var skc = req.body.skc;
     console.log('Convert file '+fname +' to DB')
		sharp(fname)
		.resize(500,500)
		.toBuffer()
		.then( data=>{
				var buf =data.toString('base64');
				var cmd="if not exists(select skc from twdpim..pimImage where SKC=@skcode)  " +
							"Insert twdpim..PimImage(SKC,BASE64) values('@skcode','@base') " +
							"else Update twdpim..Pimimage Set BASE64='@base' where SKC=@skcode";
						var pdata={
								xid:0,
								qry:cmd,
								par:{
									skcode:skc,
									base:buf
								}
								}
						db.execute(pdata,function(resp){  //-----return with req ID
							if(resp.dbcode==0){
								console.log('SAve image OK')
								res.status(200).end('image2DB Copy & Resize Image Successful')
							}
							else {
								console.log('SAve image Error ' +resp.dbmessage)
								res.sendStatus(200).end('ERR '+resp.dbmessage);
							}
								
						});
						
					})
					
		.catch( err => {
					console.log('Sharp Error '+err);
					res.send('image2DB ERR '+err)
				} 
				);


});

router.post('/BASE64TODB' , function (req, res) {
	var buff  =req.body.base64;  //'./public/uploads/userPhoto.jpg'; // req.body.fname;
	var skc = req.body.skc;
	console.log('Convert Base64  to DB')

	sharp(buff)
	.resize(500,500)
	.toBuffer()
	.then( data=>{
		var buf =data.toString('base64');
		var cmd="if not exists(select skc from twdpim..pimImage where SKC=@skcode)  " +
			"Insert twdpim..PimImage(SKC,BASE64) values('@skcode','@base') " +
			"else Update twdpim..Pimimage Set BASE64='@base' where SKC=@skcode";
		var pdata={
				xid:0,
				qry:cmd,
				par:{
					skcode:skc,
					base:buf
				}
				}
				db.execute(pdata,function(resp){  //-----return with req ID
					if(resp.dbcode==0){
						console.log('Save image OK')
						res.status(200).end('image2DB Copy & Resize Image Successful')
					}
					else {
						console.log('SAve image Error ' +resp.dbmessage)
						res.sendStatus(200).end('ERR '+resp.dbmessage);
					}
						
				});
			}) 
	.catch( err => {
					console.log('Sharp Error '+err);
					res.send('image2DB ERR '+err)
				} 
		 );		
	 
});

router.post('/TOFILE' , function (req, res) {
	 
	var imgurl  =req.body.imgurl;
	var outputFile = req.body.tofile;
	
	//var imgurl = "https://media-cdn.tripadvisor.com/media/photo-s/0d/07/48/7e/caption.jpg"
	//var outputFile ="./public/uploads/userPhoto.jpg";

	console.log('copy ['+imgurl +'] to file '+outputFile)
	//var fname=para.fname;
	//var url ="ubu.upfront.co.th./TWDPIM/web/Image/0104/60176103.jpg";
//	var url ="ubu.upfront.co.th./TWDPIM/web/"+fname;
	//var outputFile ="D:/DEvelop/Node/posbynode/public/POSIMAGE/"+skc+".jpg";
	//var outputFile ="./PRODICT/image/sku"+skc+".jpg";

	request({url: imgurl, encoding: null}, 
			function(err, result, bodyBuffer) {
				if(err){
					console.log('can not get image');
					res.end('ERR Cant get Image '+fname)
				}
				else {
					sharp(bodyBuffer).resize(500,500)
			    	.toFile(outputFile ,
			    	    function(err){
			    			if(err){
			    				  console.log(err);
			    				  res.send('ERR '+err)
			    			     }
			    			else  {
			    				console.log('Resize Image Successful');
			    				res.send('Copy & Resize Image Successful')
			    			   }
			    			}
			    	 );
				}

		});

});
 
module.exports = router