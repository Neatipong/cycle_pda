 var dbdate = require('dateformat');
 var ms = require('../resource/dbconnect'); 
 
exports.expandCtlf = function(jstr,rset,callback){   //control Json and Result Set
			expandCtlf(jstr,rset,function(ret){
				callback(ret);
			  });
};
 
exports.expandOStr = function(cstr,callback){ //--input as string control patter
	expandOstr(cstr,function(xid,ret){
		callback(xid,ret);
	  });
};

exports.expandCstr = function(jstr,callback){ //--input as Json control patter
	expandCstr(jstr,function(ret){
		callback(ret);
	  });
};

function expandOstr(cstr,callback){
	var tabctl={
			hdr:null,
			bdy:null,
			qry:null,
			cmd:null,
			erx:"Error @error",
			end:null
	};
 //console.log('xpandOstr '+cstr);
	
	var xid='NONE';
	try { 
	var lns=cstr.split("|");
	for(var n=0;n<lns.length;n++)
		{
		var str =lns[n];

		if(str.substring(0,3)=="XID" )xid =str.substring(4);
		if(str.substring(0,3)=="HDR" )tabctl.hdr=str.substring(4);
		if(str.substring(0,3)=="SQL" )tabctl.qry=str.substring(4);
		if(str.substring(0,3)=="BDY" )tabctl.bdy=str.substring(4);
		if(str.substring(0,3)=="CMP" )tabctl.cmp=str.substring(4);
		if(str.substring(0,3)=="END" )tabctl.end=str.substring(4);
		if(str.substring(0,3)=="ERX" )tabctl.erx=str.substring(4);
		}
	xid = xid.trim();
	tabctl.xid=xid;
	
	    expandCstr(tabctl,function(htm){
	    //	console.log('XpandOstr REt '+ htm);
	    	callback(xid,htm);
	    });
	}
	catch(ex){
		console.log('XpandOstr err  '+ ex.message);
		callback(xid,'XpandOstr  Error...'+ex.message); 
	}
}
//-----------Control string in the form of json
function expandCstr(ctlf,callback){
	
 var hdrs	=ctlf.hdr;
 var qrys	=ctlf.qry;
 var bdys	=ctlf.bdy;
 var ends	=ctlf.end;
 var cmps 	=ctlf.cmp;
 var errs	=ctlf.erx;
 var xid	=ctlf.xid;
 
 if(qrys==' '){
	    callback(hdrs+bdys+ends);
	    return; 
   }
 
 if ((typeof(xid)==='undefied')||
 		(xid===null))xid="NONE";
 		
 if  ((typeof(errs)==='undefied')||
       (errs===null) )errs="@error";
   
 //-------------get Query
	ms.dbGetQuery(qrys, function(resp){
	//console.log('ret from getquery ..'+qrys);
 	//return;
		
	try { 
	    var emsg=resp.dbmessage;
	    var ecde=resp.dbcode;
		var rset =resp.dbitems;  //--result set from DB
	//console.log(resp);
		//return;
 	   if(ecde>0){	 //----get data with ERROR
 		    console.log('Read db '+qrys +'\nError: '+emsg);
 		    var estr =errs.replace('@error',emsg);
 		    var htm= estr;
			 console.log(htm);
			 callback(htm);
			 return;
			 }
		else {
			
			//--------SEnd result set to Xpand
			ctls={hdr:hdrs,
				  bdy:bdys,
				  erx:errs,
				  cmp:cmps,
				  end:ends}
		//console.log('expand Ctlf ')
			     expandCtlf(ctls,resp,function(htm){ //control string  + result set
				  callback(htm);
				  return;
			});
		 
		  }
		}
		catch(ex){
			console.log('XpandCstr err  '+ ex.message);
			callback('xpands.js  Error...'+ex.message); 
		}
  	});
	
  
 
}
 
function expandCtlf(ctlf,resp,callback){

console.log('Call xpandCtlf');

 var hdrs	=ctlf.hdr;
 var bdys	=ctlf.bdy;
 var ends	=ctlf.end;
 var cmps 	=ctlf.cmp;
 var errs	=ctlf.erx;
 var rset 	=resp.dbitems;  //--result set from DB
 var flds 	=resp.dbfields;
 /*
 if(hdrs==''){
	    callback( bdys+ends);
	    return; 
}
*/
// console.log('Rset..'+typeof(rset));
if ((errs===null)||(typeof(errs)==='undefined'))errs="Error .. @error ";	

 if ( (typeof(rset)==='undefined')||(rset===null)){
	// callback('expandCtlf Error .. No result set pass through');
	 var html=hdrs + errs.replace("@error","No Result from DB") +ends ;
	 // var html ="No Result from DB" ;
	 //var html=hdrs+ errs +ends 
	 callback(html);
	 return;
 }
  var nrw =rset.length;
		if(nrw==0){
			  var html= hdrs + errs.replace("@error","No Result from DB")+ends  ;
			//  var html="No Result from DB";
			 callback(html);
			 return;
	       }
		 else {
				var html=hdrs;
			   for(var rw =0;rw<nrw;rw++){
				   var pat =bdys;
					var row=rset[rw];
					var  coln =100;
					for(key in row){
						var colns =""+coln;
						colns=colns.replace("1",":");
						var val =row[key];
						if(val==null)val=''; 
						
						//--------Check type of val to convert Datetime
						if(Array.isArray(flds) ){ 
						var cty =flds[0][key];     //problem between mssql and mysql 

		 // console.log('CTY ='+cty);
		  
						if(typeof cty =='string'){    //--mssql only 
						cty=cty.toLowerCase();
						
						// ---convert problem with time Zone !!!
						/*** for mssql datetime is depend on OS Host of webpage ?? , you must set timeCtl time zone in ubutu
							if(cty.indexOf('datetime')>=0){
								//---problem with mssql on cloud, time is GMT time
							  val =dbdate(val,'yyyy-mm-dd HH.MM');  //- !!return diff val depned on where this web is!!!
							  
							  //local data format this work when node is on window, But not on UBUNTU???
							   // val = dbdate(val,'isoDateTime');  //not work
							   //	console.log(key +' '+cty);
								//---this seem to work
							   // val	=val.toLocalString();
							   // var dstr =val.toISOString();
							   // val =dstr.substring(0,16).replace('T',' ');;
							 }
						*/	 
							 // datetimen  is Global time ?????????
						  if((typeof val  =='object') ||   cty.indexOf('datetimen')>=0){
						 //  if(cty.indexOf('datetime')>=0){
 							//---this seem to work
						   
						    	//var dstr =val.toISOString(); //--if you run on Cloud OK, but local time+5 Hr  NOK???
						    	//dstr =dstr.substring(0,16);
								//val =dstr.replace('T',' ').replace(':','.');

								 var dstr =dbdate(val,'yyyy-mm-dd HH:MM');  //- !!return diff val depned on where this web is!!!
								 var d =new Date(dstr);
								 d.setHours( d.getHours() -7  );   //?????? Add Bangkok Time ????
						//		 console.log('Lcal date '+dbdate(d,'yyyy-mm-dd HH:MM'));
							    val = dbdate(d,'yyyy-mm-dd HH.MM')

						      }
						      
							
						   }
						}
						 
						var rp  =RegExp(colns,'g');
						
						 pat=pat.replace(rp,val);
						 coln+=1;
						 //---if specified colname
						 var knm ="\\^"+ key + "\\^";  //^ ^ is special charater, also \, this is how to do escape spc
					
						 //rp  =RegExp(knm,'g');   //??special cahracter cocna not be used here
						 var regx =RegExp(knm,'g');
						 pat=pat.replace(regx,val);
					     
					}
				//	console.log(pat);
					html+=pat;
			   }
			   //---add comute if any
			  // console.log('Try to compute..');
			  
			   if( cmps  != null){ 
				   try { 
				   var pos=cmps.indexOf('@SUM(');
				   while(pos>0){
					  var eps =cmps.indexOf(')',pos);
					  if(eps<=0)break;
					  
					  var  coln=cmps.substr(pos+5,eps-pos-5);
					  var val = sumArray(rset,coln);
					  var key='@SUM('+coln+')';
					  cmps=cmps.replace(key,val);
					//  console.log('sum on col '+coln +' val ='+val);
					  pos=cmps.indexOf('@SUM(',eps+1);
				    }
				   }
				  catch(ex){cmps="Error Try to Compute "+ex.message; }
				  
			   html+=cmps;
			   }
			   
			   html+=ends;
			   
			  // console.log('get back from xpand.js');
				callback(html);
			}
 
}

function sumArray(rset,coln){
	var tot =0;
	  for(var rw =0;rw<rset.length;rw++){
		  tot+=rset[rw][coln];
	  }
	return(tot);
}
	
 

 