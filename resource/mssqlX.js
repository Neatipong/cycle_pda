
exports.msGetQuery = function(sql,callback){
			//-----if qry contain http: then it is rmdatabase
			if(sql.indexOf('http://')>=0){
				  getRmQuery(sql,function(ret){
						callback(ret);
					});	
			}
			else
				getQuery(sql,function(ret){
				callback(ret);
				});
};
var http=require('http')
const querystring = require('querystring');
//-------remote call to dbGateway ------
function getRmQuery(sql,callback){
	//console.log(sql);
	   var resp ={
				dbcode:999,
				dbmessage:"Start",
				dbfileds:null,
		   		dbitems:null
             }
	try { 
	   var host=sql.split("^")[0];
	   var qry =sql.split("^")[1];
	   host=host.trim();	   
	   console.log(host);

	   var pdata={query:qry};

	   const request = require('request')
	   request.post({
		  url:     host,
		  body:  querystring.stringify(pdata)
		}, function(error, response, body){  //response return the wole response
			//	console.log(error);
			var jsn=JSON.parse(body);
		 //	 console.log(jsn);
			//------Convert to Our Format
			resp.dbcode		=jsn.Error.code;
			resp.dbmessage	=jsn.Error.message;
			resp.dbitems	=jsn.Item;
		//	console.log(resp);
	   		callback(resp);
		}).setHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');

	}
catch(ex){
	console.log(ex.message);
	resp.dbmessage=ex.message;
	callback(resp);}
}
//-----------MSSQL QUERY-------
const config = require('config');
const dbConfig = config.get('mssqlConfig');
function   getQuery(sql,callback){
	//console.log(sql);
	
	var Connection 	= require('tedious').Connection;
	var Request 	= require('tedious').Request; 
	var dbName		= dbConfig.dbName;
	
	var config = {
	    server: dbConfig.host, //"vpn.upfront.co.th",
		char_set:"utf8",
	    // If you're on Windows Azure, you will need this:
	    options:{ database:dbName,
	    		encrypt: true
	    		},
	    authentication: {
			    type: "default",
			    options: {  
			        userName: dbConfig.userid, //"upfdba",
			        password: dbConfig.password //"upfrontadmin"
			      }
				 }
	     }

 var connection = new  Connection(config); 
	connection.on('eror'
      , function(err) {
				console.log("MSSQL Connect err...\n"+err);
			     var resp ={
							dbcode:999,
							dbmessage:err,
					   		dbitems:null
			              }
				callback(resp);
			     return;
	        });
      
  connection.on('connect'
	, function(err) {
		if(err){
			console.log("MSSQL Connect err...\n"+err);
			  var resp ={
						dbcode:999,
						dbmessage:err,
				   		dbitems:null
				   }
				  callback(resp);
			  return;
			}
		
    var results = []; 
    var fields	=[];
    var ecode =0;
    var emsg="OK";
    
    var request = new Request(sql
			,function(err, rowCount) {
			  if (err) {
					ecode=err['number'];
			    	emsg=err['message'];
			    	console.log('error ..'+emsg +' \ndatabase '+dbName);
			  } else {
					console.log('Read db '+ rowCount + ' rows');
				}
			});
    
    	request.on('columnMetadata', function (columns) { 
    		var item = {}; 
			columns.forEach(function (column) { 
				item ={ cname:column.colName,
						ctype:column.type.name,
						clen:column.dataLength
						}
				}); 
				fields.push(item); 
    	});
    	
      request.on("row", function (columns) { 
			var item = {}; 
			columns.forEach(function (column) { 
				item[column.metadata.colName] =column.value
				}); 
				results.push(item); 
		    }); 
      
      request.on("error", function (err) { 
    	  ecode=err['number'];
    	  emsg=err['RequestError'];
    	  console.log('error ..'+emsg +' \ndatabase '+dbName);
		    }); 
      
      request.on('requestCompleted',function(){
    	  var resp ={
 				 dbcode:ecode,
 				 dbmessage:emsg,
 				 dbfileds:fields,
 				 dbitems:results
 	   			}
    	  console.log(fields);
    	  	callback(resp);
      	}); 
      
      connection.execSql(request);
	});
  
  
};

