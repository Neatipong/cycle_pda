﻿
function saveCookie(name, value, days) {
    var date = new Date();

    if (days) 
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));  //n days
    else 
    	date.setTime(date.getTime() + (10* 60 * 60 * 1000));   //106 hour 
    
      var expires = "; expires=" + date.toUTCString(); 	
    //	var expires = "";
     document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function removeCookie(name) {
    saveCookie(name, "", -1);
}

function str2Number(str) {
    if (str == null || str == ' ') return (0);
    var ret = str;
    try {
        ret = str.replace(/,/g, "");
    }
    catch (e) { ret = 0; }

    if (isNaN(ret)) return (0);
    else return ret * 1;
}


/***/
//-------------------
function numericFormat(amt) {
    var sgn = "";
    if (isNaN(amt)) return (".00");

    var ostr = new String(".00");
    var astr = new String(amt);
    try {        
        astr = astr.replace(/,/g, "").replace(/[\n\r]/g, "");

        if (astr.substring(0, 1) == "-") {
            astr = astr.substring(1)
            sgn = "-";
        }
        var iv = parseInt(astr);
        var mn = new String(iv);

        // ostr=mn;
        var dc = astr.indexOf(".", 1);
        if (dc > 0) { astr = astr + '00'; ostr = astr.substr(dc, 3); }
        var ln = mn.length;
        for (var i = 0; i < ln; i++) {
            if ((i == 3 && ln > 3) || (i == 6 && ln > 6) || (i == 9 && ln > 9)) ostr = "," + ostr;
            ostr = mn.substr(ln - 1 - i, 1) + ostr;
        }
    }
    catch (Error) { alert(Error); }
    return sgn + ostr;
}
function yyyymmdd() {
    var x = new Date();
    var y = x.getFullYear().toString();
    var m = (x.getMonth() + 1).toString();
    var d = x.getDate().toString();
    (d.length == 1) && (d = '0' + d);
    (m.length == 1) && (m = '0' + m);
    var yyyymmdd = y + m + d;
    return yyyymmdd;
}

 //---convert numeric format
 function setNumericFormat(){
    setNumericClass();
 }
function setNumericClass() { 
  $('.money').each(function(){
         var val=$(this).text();
         val = val.replace(/,/g, "");
         $(this).css('text-align','right');
         val=val.trim();
         if (val.length>0)
		 $(this).html(numericFormat(val))
      });	
      $('.numeric').each(function(){
        var val=$(this).text();
        val = val.replace(/,/g, "");
        $(this).css('text-align','right');

        val=val.trim();
        if (val.length>0)
        $(this).html(numericFormat(val))
     });	
 }
 function setNumericFormatDiv(div) { 
    $('#'+div+' .numeric').each(function(){
      var val=$(this).text();
      val = val.replace(/,/g, "");

      $(this).css('text-align','right');

      val=val.trim();
      if (val.length>0)
      $(this).html(numericFormat(val))

      
   });	
}
 //???? This function dont work on nonsecure web page !!!!!
function getGeoLocation(callback) {
	try{ 
        
        if (navigator.geolocation) {            
            navigator.geolocation.getCurrentPosition(showPosition);
          } else {                       
           callback(false,"Geolocation is not supported by this browser.");
          }
        }
  catch(ex){
     callback(false,ex.message);
    }
 
function showPosition(position) {    
    callback(true, position.coords.latitude +"," + position.coords.longitude);
   }
}

const RoundTypeEnum = {
    Up: 0,
    Down: 1
}

function quarterRound(value, roundType) {
	var baht, stang;

	baht = Math.floor(value);
	stang = (value * 100) % 100;

	if (roundType === RoundTypeEnum.Up) {
		if (stang > 0 && stang < 25) {
			stang = 25;
		} else if (stang > 25 && stang < 50) {
			stang = 50;
		} else if (stang > 50 && stang < 75) {
			stang = 75;
		} else if (stang > 75 && stang < 100) {
			baht = baht + 1;
			stang = 0;
		}
	} else {
		if (stang > 0 && stang < 25) {
			stang = 0;
		} else if (stang > 25 && stang < 50) {
			stang = 25;
		} else if (stang > 50 && stang < 75) {
			stang = 50;
		} else if (stang > 75 && stang < 100) {
			stang = 75;
		}
	}

	return (baht + (stang / 100));
}