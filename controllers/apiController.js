const  dbconnect = require('../resource/callsql');
const jwt = require('jsonwebtoken');
const jwtsecret = "jwtcyclecountpda";

exports.login = async (req, res, next) => {
    try {
        
        console.log("-------------------------------start login-----------------------------")
        const { USERID, PASSWORD } = req.body;
        let qry =`exec  CCTSQL..WebCC_CheckUserLogin2 '${USERID}','${PASSWORD}',''`;
        console.log(qry)
        let getsql_WebCC_CheckUserLogin2 = await dbconnect.getquery(qry);
        
       
        let {RESULT ,SVRDATE, USERGROUP, GroupName, CHGPWDDATE, PRICEORCOST} = getsql_WebCC_CheckUserLogin2.dbitems[0];
        if(RESULT=="OKOK"){

            let now = new Date();
            let chgpwdDate = new Date(CHGPWDDATE);
            const diffTime = Math.abs(chgpwdDate - now);
            let lastChgPwdDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            let getLOGINID='';
            if(lastChgPwdDays < 90){
                let qry = `update SYSTEMUSER 
                    set INVALIDPWD=0,
                    LASTLOGIN=getdate()
                    where USERID = '${USERID}';
                    exec WebCC_LoginProc '${USERID}';
                    `;
                let getsql_update_SYSTEMUSER = await dbconnect.getquery(qry);
                console.log("xxxxxxxxxxxxxxxxxxxxxxxxxxxgetsql_update_SYSTEMUSERxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
                console.log(getsql_update_SYSTEMUSER)
                getLOGINID = getsql_update_SYSTEMUSER.dbitems[0].LOGINID;
                
                
            }
            qry = `SELECT right('0000'+convert(varchar,value),4) as PF_NUM from openjson((Select USER2PROFILE From SYSTEMUSER Where USERID = '${USERID}'))`;
            let getsql_USER2PROFILE = await dbconnect.getquery(qry);
            console.log(getsql_USER2PROFILE)
            let user2profile = '';
            getsql_USER2PROFILE.dbitems.forEach(item => user2profile += `X${item.PF_NUM}`);
            const token = await jwt.sign({
                CCUSERID: USERID,
                CCUGROUP: USERGROUP,
                CCPRICEORCOST: PRICEORCOST,
                CCGROUPNAME: GroupName,
                CCLSTCHGPWDDAYS: lastChgPwdDays,
                CCUPROFILE:user2profile,
                CCLOGINID:getLOGINID
            }, jwtsecret  , { expiresIn: '1 days' });
            
            //สร้าง token

 
            //decode วันหมดอายุ
            const expires_in = jwt.decode(token);
            console.log("-------------------------------end login-----------------------------")
            return res.status(200).json({
                access_token: token,
                expires_in: expires_in.exp,
                token_type: 'Bearer'
            }); 
        }else{
            throw new Error(RESULT)
        }

    } catch (error) {
       next(error); 
    }
}
exports.getstorebyuser = async (req, res, next) => {
    try {
        
        console.log("-------------------------------start getstorebyuser-----------------------------")
        // {
        //     CCUSERID: 'MISUPF',
        //     CCUGROUP: '008',
        //     CCPRICEORCOST: 'C',
        //     CCGROUPNAME: 'MIS',
        //     CCLSTCHGPWDDAYS: 8,
        //     CCUPROFILE: 'X1101X1102X1103X1104X1105X1106X1107X1108X1109X1110X1111X1112X1113X1114X1115X1116X1117X1118X1119X1120X1121X1122X1123X1124X1125X1126X1127X1129X1130',
        //     iat: 1613535739,
        //     exp: 1613622139
        //   }
 
        let qry =`exec CCTSQL..WebCC_LoadStoreByUser '${req.user.CCUSERID}'`;
        let getsql = await dbconnect.getquery(qry);
 
        return res.status(200).json({
            status_code: 200,
            message: "OK",
            data: getsql
        }); 

    } catch (error) {
       next(error); 
    }
        
}

exports.countname = async (req, res, next) => {
    try {
        
        console.log("-------------------------------start countname-----------------------------")
        const { STCODE} = req.body;
        let qry =`exec CCTSQL..PDACC_ListCntMainByStore '${STCODE}'`;
        let getsql = await dbconnect.getquery(qry);
 
        return res.status(200).json({
            status_code: 200,
            message: "OK",
            data: getsql
        }); 

    } catch (error) {
       next(error); 
    }
        
}

exports.scan = async (req, res, next) => {
    try {
        console.log("-------------------------------start scan-----------------------------")
        const { MOD,CNTTRANS,CNTNUM,SKCODE,PRCODE,LOCATION,CNTQNT,USERID,HOSTNAME  } = req.body;
        let qry =`declare @result varchar(500)
              exec CCTSQL..PDACC_SaveKeyCount  '${MOD}', ${CNTTRANS},'${CNTNUM}',${SKCODE},${PRCODE ? `'${PRCODE}'` : null},'${LOCATION}','${CNTQNT}', '${USERID}','${HOSTNAME}', @result output
              select @result as RESULT`;
              console.log(qry)
        let getsql = await dbconnect.getquery(qry);
        console.log("-------------------------------get scan-----------------------------")
        console.log(getsql)
        return res.status(200).json({
            status_code: 200,
            message: "OK",
            data: getsql
        }); 

    } catch (error) {
       next(error); 
    }     
}

exports.listedit = async (req, res, next) => {
    try {
        console.log("-------------------------------start listedit-----------------------------")
        const { CNTNUM,USERID,LOCATION,SKUBAR} = req.body;
        let qry =`exec CCTSQL..PDACC_ListCntTrans4Edit '${CNTNUM}', '${USERID}', ${LOCATION ? `'${LOCATION}'` : null}, ${SKUBAR ? `'${SKUBAR}'` : null}`;
              console.log(qry)
        let getsql = await dbconnect.getquery(qry);
        console.log("-------------------------------get listedit-----------------------------")
        console.log(getsql)
        return res.status(200).json({
            status_code: 200,
            message: "OK",
            data: getsql
        }); 

    } catch (error) {
       next(error); 
    }     
}

exports.listitemnotcount = async (req, res, next) => {
    try {
        console.log("-------------------------------start listitemnotcount-----------------------------")
        const { CNTNUM ,MODE} = req.body;
        //1 onhand
        let qry =`exec CCTSQL..PDACC_ListItemNotCount '${CNTNUM}',${MODE}`;
              console.log(qry)
        let getsql = await dbconnect.getquery(qry);
        console.log("-------------------------------get listitemnotcount-----------------------------")
        console.log(getsql)
        return res.status(200).json({
            status_code: 200,
            message: "OK",
            data: getsql
        }); 

    } catch (error) {
       next(error); 
    }     
}

exports.getproductinfo = async (req, res, next) => {
    try {
        console.log("-------------------------------start getproductinfo-----------------------------")
        const { KEYSRC , STCODE} = req.body;
        let qry =`exec CCTSQL..PDACC_GetProduct4Count '${KEYSRC}', '${STCODE}'`;
              console.log(qry)
        let getsql = await dbconnect.getquery(qry);
        console.log("-------------------------------get getproductinfo-----------------------------")
        console.log(getsql)
        return res.status(200).json({
            status_code: 200,
            message: "OK",
            data: getsql
        }); 

    } catch (error) {
       next(error); 
    }     
}

exports.editvariance = async (req, res, next) => {
    try {
        console.log("-------------------------------start editvariance-----------------------------")
        const { CNTNUM,SKUBAR,LOCATION} = req.body;
        let qry =`exec CCTSQL..PDACC_ListItem4EditVariance '${CNTNUM}', ${SKUBAR ? `'${SKUBAR}'` : null}`;
              console.log(qry)
        let getsql = await dbconnect.getquery(qry);
        console.log("-------------------------------get editvariance-----------------------------")
        console.log(getsql)
        return res.status(200).json({
            status_code: 200,
            message: "OK",
            data: getsql
        }); 

    } catch (error) {
       next(error); 
    }     
}

exports.checklocation = async (req, res, next) => {
    try {
        console.log("-------------------------------start checklocation-----------------------------")
        const { CNTNUM,LOCATION} = req.body;
        let qry =`declare @res varchar(500)
        exec WebCC_STCKCNT_CheckCntLocation '${CNTNUM}', '${LOCATION}', @res output
        select @res as RESULT`;
              console.log(qry)
        let getsql = await dbconnect.getquery(qry);
        console.log("-------------------------------get checklocation-----------------------------")
        console.log(getsql)
        return res.status(200).json({
            status_code: 200,
            message: "OK",
            data: getsql
        }); 

    } catch (error) {
       next(error); 
    }     
}

exports.addcntlocation = async (req, res, next) => {
    try {
        console.log("-------------------------------start addcntlocation-----------------------------")
        const { CNTNUM,LOCATION,USERID} = req.body;
        let qry = `declare @res varchar(500)
        exec WebCC_STCKCNT_AddCntLocation '${CNTNUM}', '${LOCATION}', '${USERID}', @res output
        select @res as RESULT`;
              console.log(qry)
        let getsql = await dbconnect.getquery(qry);
        console.log("-------------------------------get addcntlocation-----------------------------")
        console.log(getsql)
        return res.status(200).json({
            status_code: 200,
            message: "OK",
            data: getsql
        }); 

    } catch (error) {
       next(error); 
    }     
}
exports.getrtv = async (req, res, next) => {
    try {
        console.log("-------------------------------start getrtv-----------------------------")
        const {SKU,STCODE} = req.body;
        console.log("🚀 ~ file: apiController.js ~ line 270 ~ exports.getrtv= ~ STCODE", STCODE)
        console.log("🚀 ~ file: apiController.js ~ line 270 ~ exports.getrtv= ~ SKU", SKU)
        let qry = `exec PDACC_GetRTVInstruction '${STCODE}', '${SKU}'`;
              console.log(qry)
        let getsql = await dbconnect.getquery(qry);
        console.log("-------------------------------get getrtv-----------------------------")
        console.log(getsql)
        return res.status(200).json({
            status_code: 200,
            message: "OK",
            data: getsql
        }); 

    } catch (error) {
       next(error); 
    }     
}
exports.logout = async (req, res, next) => {
    try {
        console.log("-------------------------------start logout-----------------------------")
        const { LOGINID } = req.body;
        let qry = `exec WebCC_LogoutProc ${LOGINID}`;
              console.log(qry)
        let getsql = await dbconnect.getquery(qry);
        console.log("-------------------------------get logout-----------------------------")
        console.log(getsql)
        return res.status(200).json({
            status_code: 200,
            message: "OK",
            data: getsql
        }); 

    } catch (error) {
       next(error); 
    }     
}