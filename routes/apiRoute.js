var express = require('express');
const jwt = require('jsonwebtoken');
const passportJWT = require('../middleware/passportJWT');
var router = express.Router();
const pageController = require('../controllers/pageController');
const apiController = require('../controllers/apiController');
router.route("/login")
    .post(apiController.login)
    // .post([passportJWT.isToken],apiController.login)
router.route("/getstorebyuser")
    .post([passportJWT.isToken],apiController.getstorebyuser)
router.route("/countname")
    .post([passportJWT.isToken],apiController.countname)
router.route("/scan")
    .post([passportJWT.isToken],apiController.scan)
router.route("/listedit")
    .post([passportJWT.isToken],apiController.listedit)
router.route("/listitemnotcount")
    .post([passportJWT.isToken],apiController.listitemnotcount)
router.route("/getproductinfo")
    .post([passportJWT.isToken],apiController.getproductinfo)
router.route("/editvariance")
    .post([passportJWT.isToken],apiController.editvariance)
router.route("/checklocation")
    .post([passportJWT.isToken],apiController.checklocation)
router.route("/addcntlocation")
    .post([passportJWT.isToken],apiController.addcntlocation)
router.route("/getrtv")
    .post([passportJWT.isToken],apiController.getrtv)
router.route("/logout")
    .post([passportJWT.isToken],apiController.logout)
module.exports = router;


