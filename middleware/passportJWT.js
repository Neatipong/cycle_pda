

const passport = require('passport');

const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

//opts.issuer = 'accounts.examplesoft.com';
//opts.audience = 'yoursite.net';
const cookieExtractor = req => {
    let jwt = null 

    if (req && req.cookies) {
        jwt = req.cookies['CCTOKEN']
    }

    return jwt
}
const jwt_cookie_opts = {}
const secretOrKey = "jwtcyclecountpda"
// opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwt_cookie_opts.jwtFromRequest =  cookieExtractor;
jwt_cookie_opts.secretOrKey = secretOrKey;
passport.use('jwt-cookie',new JwtStrategy(jwt_cookie_opts, async (jwt_payload, done) => {
    try {

        const { expiration } = jwt_payload

        if (Date.now() > expiration) {
            done(new Error('หมดอายุ'), null)
        }

       return done(null, jwt_payload);

    } catch (error) {
        done(error);
    }
}));


const jwt_token_opts = {}
jwt_token_opts.jwtFromRequest =  ExtractJwt.fromAuthHeaderAsBearerToken();
jwt_token_opts.secretOrKey = secretOrKey;
passport.use('jwt-token',new JwtStrategy(jwt_token_opts, async (jwt_payload, done) => {
    try {

        const { expiration } = jwt_payload

        if (Date.now() > expiration) {
            done(new Error('หมดอายุ'), null)
        }

       return done(null, jwt_payload);

    } catch (error) {
        done(error);
    }
}));

module.exports.isLogin = (req, res, next) => {
    passport.authenticate('jwt-cookie', function(err, user, info) {
      if (err) return next(err);
      if (!user){
        const error = new Error('Unauthenticate');
        error.statusCode = 401;
        error.pageurl='/';
        throw error
      } 
      req.user = user;
      next();
    })(req, res, next);
}

module.exports.isToken = (req, res, next) => {
    passport.authenticate('jwt-token', function(err, user, info) {
      if (err) return next(err);
      if (!user){
        const error = new Error('Unauthenticate');
        error.statusCode = 401;
        throw error
      } 
      req.user = user;
      next();
    })(req, res, next);
}
// module.exports.isLogin = passport.authenticate('jwt-cookie', {  failureRedirect: '/',session: false });
// module.exports.isLoginWithHeader = passport.authenticate('jwt', { session: false });

// module.exports.isLoginWithCookie = function (req, res, next) {
//     console.log('LOGGED')
//     next()
//   }
  
