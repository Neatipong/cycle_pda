/**
 * This function support xpand sercie the old php veriosn
 * XPANDPAT
 * XPANDTAB
 * XPANDFILE
 */
var express 	= require('express')
var router 		= express.Router()
var bodyParser 	= require('body-parser')
var dt 			= require('../resource/datetime');
var fs 			=require("fs");
var ms			= require('../resource/dbconnect');
var dbdate = require('dateformat');
//var Step		= require('step');  //dont work for  sync
 

router.use(function timeLog (req, res, next) {
  console.log('Old Xpand service at Time: ', dt.myDateTime())
  next()
})


router.use(express.json());
 
var xpd = require('../resource/xpand');
var async = require('async');
 
router.post('/' , function (req, res) {
	 console.log('Call Xpand service '+req.body);
     var func 	=req.body.FUNC;
     var para	=req.body.PARA;
     var ctlf 	=req.body.CTLF
     
     switch(func){
     case "XPANDTAB" :
     		res.status(404).send("FUNCTION is not Supported");
     		return;
     case "XPANDFILE":
			res.status(404).send("FUNCTION is not Supported");
     		return;
     case "XPANDPAT":
     		//ctlf is string contain $HDR $SQL ...
     		expandOpat(ctlf,para,function(ret){
     		     res.send(ret);
     		});
     }
	}   
  ); 

function expandOpat(ctlstr,para,callback){
	 var rethtm;
 
	 var patctl={
			 hdr:null,
			 bdy:null,
			 qry:null,
			 erx:"Error @error ",
			 cmp:null,
			 pre:null,
			 end:null
	 };

	  //------replace  para
	   for(key in para){
			var val =para[key];
			var rp  =RegExp('@'+key,'g');
			 ctlstr=ctlstr.replace(rp,val);
			}
	    //-----Change string to JSON control
		var lns=ctlstr.split("$");
 
		for(var n=1;n<lns.length;n++)
			{
			var str =lns[n];
			if(str.substring(0,3)=="HDR" )patctl.hdr=str.substring(4);
			if(str.substring(0,3)=="SQL" )patctl.qry=str.substring(4);
			if(str.substring(0,3)=="BDY" )patctl.bdy=str.substring(4);
			if(str.substring(0,3)=="END" )patctl.end=str.substring(4);
			if(str.substring(0,3)=="ERX" )patctl.erx=str.substring(4);
			}
	 
	  xpd.expandPstr(patctl,function(htm){ //control string  + result set
    		 callback(htm);
      });	
}
	 
 

module.exports = router