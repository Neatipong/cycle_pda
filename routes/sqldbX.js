var express 	= require('express')
var router 		= express.Router()
var bodyParser 	= require('body-parser')
var dt 			= require('../resource/datetime');

var ms 			= require('../resource/mssql');
// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  console.log('call MsSQLDB at Time: ', dt.myDateTime())
  next()
})


router.use(express.json());
//router.use(express.urlencoded());
//router.use(express.multipart());
 /*
router.post('/QUERY' , function (req, res) {
	//console.log("Request Body...");
	//console.log(req.body);
	//return;
	if(!req.body.query){
		res.status(200).json({
				error:{ 
					message:'query is not defined'  ,
					code:99
				},
				resp:null,
				time:dt.myDateTime()
			})
		return; 
	}
     var qry=req.body.query;
     	ms.msGetQuery(qry,function(resp){
     		res.status(200).json({
				error:{ 
					message:resp.dbmessage ,
					code:resp.dbcode,
				   },
				resp:resp,
				time:dt.myDateTime()
				})
     	});

});
router.post('/POSREQ' , function (req, res) {
	console.log("Request Body...");
	console.log(req.body);
	//return;
	if(!req.body.query){
		res.status(200).json({
			error:{ 
				message:'query is not defined'  ,
				code:99
			},
			resp:null,
			time:dt.myDateTime()
		})
	return; 
	}
     var qry=req.body.query;
     	ms.msGetQuery(qry,function(resp){
     		console.log(resp);
     		res.status(200).json({
				error:{ 
					message:resp.dbmessage ,
					code:resp.dbcode,
				   },
				resp:resp,
				time:dt.myDateTime()
				})
     	});

});
//???????Not USe anymore ???
var xpd = require('../resource/xpand');
//---request is  json wiht hdr,sql,bdy,erx,end
router.post('/XPAND' , function (req, res) {
	//console.log(req.body);
	if(!req.body.qry){
		    res.send("No Query specified");
		    return; 
	  }
     var hdrs=req.body.hdr;
     var qrys=req.body.qry;
     var bdys=req.body.bdy;
     var cmps=req.body.cmp;
     var ends=req.body.end;
     var errs=req.body.erx;
    // console.log(qrys);
     ms.msGetQuery(qrys, function(resp){
    	//console.log(resp);
    	//return;
 	    var emsg=resp["dbmessage"];
	    var ecde=resp["dbcode"];
		var ret =resp["dbitems"];
		//console.log(ret);
		//return;
    	 if(ecde>0){		
				var html=hdrs + errs.replace("@error",emsg) +ends;
				res.send(html);
				}
		else {
			
			//--------SEnd result set to Xpand
			ctls={hdr:hdrs,
				  bdy:bdys,
				  erx:errs,
				  cmp:cmps,
				  end:ends}
			
			xpd.expandPat(ctls,ret,function(htm){ //control string  + result set
				res.send(htm);
			});
		 
		  }
     	});
     
     });

router.post('/QRY2TAB' , function (req, res) {
	//console.log(req.body);
	if(!req.body.query){
		    res.send("No Query specified");
		    return; 
	  }
     var qrys=req.body.query;
     ms.msGetQuery(qrys, function(resp){
 	    var emsg=resp["dbmessage"];
	    var ecde=resp["dbcode"];
		var rset =resp["dbitems"];
		//console.log(res);
		
        var htm="";
    	if(ecde>0){		
				html="<h4>Error "+emsg +"</h4>";
				res.send(html);
				}
		else {
			
		    var nrw=rset.length;
		    
		     if(nrw==0){
				var html= "<h4>No Result from DB</h4>" ;
				res.send(html);
	            }
		    else {
		    	var hdr="<table><thead>";
				 
				 for(key in rset[0]){
					 hdr+="<th>"+key+"</th>";
				    }
				 hdr+="</thead>";
			 
				 for(var rw =0;rw<nrw;rw++){
					    var pat ="<tr>";
						var row=rset[rw];
						for(key in row){
							var val =row[key];
						    pat+="<td>"+val+"</td>";   
						}
				 
					 htm += pat+"</tr>";
				   }  
			 
			   res.send(hdr+htm+"<table>");
		    }
		}
		  });
     
     });
*/
module.exports = router