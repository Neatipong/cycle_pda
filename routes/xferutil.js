var express = require("express");

var router = express.Router();
var dt = require("../resource/datetime");
var request = require("request");
var db = require("../resource/dbconnect");
var mydb = require("../resource/mysqlconnect");
const sharp = require("sharp");

router.use(function timeLog(req, res, next) {
  console.log("XferUtil : ", dt.myDateTime());
  next();
});
//-----------------Copy Image to Database
router.get("/IMAGE", function (req, res) {
  var query = require("url").parse(req.url, true).query;

  var imgurl = query.imgurl;
  var skc = query.skc;
  var lupd = query.lupd;
  var enrich = query.enrich;
  // res.status(200).send(imgurl);

  request({ url: imgurl, encoding: null }, function (err, result, bodyBuffer) {
    if (err) {
      console.log("can not get image");
      res.end("ERR Cant get Image from url ");
    } else {
      sharp(bodyBuffer)
        .resize(300, 300)
        .toBuffer()
        .then((data) => {
          var buf = data.toString("base64");
          //var cmd="if not exists(select skc from pimImage where SKC=@skcode)  " +
          //			 "Insert PimImage(SKC,BASE64) values('@skcode','@base')";

          var cmd =
            "if not exists(select skc from twdpim..pimImage where SKC=@skcode)  " +
            "Insert twdpim..PimImage(SKC,BASE64,LastUpdatePIM,UpdateDate, Enrich) values('@skcode','@base','@lupd', getdate(), '@enrich') " +
            "else Update twdpim..Pimimage Set BASE64='@base', LastUpdatePIM='@lupd', UpdateDate=getdate(), Enrich='@enrich' where SKC=@skcode";

          var pdata = {
            xid: 0,
            qry: cmd,
            par: {
              skcode: skc,
              base: buf,
              lupd: lupd,
              enrich: enrich
            },
          };
          db.execute(pdata, function (resp) {
            //-----return with req ID
            if (resp.dbcode == 0) {
              res.send("IMG2DB OK " + skc);
            } else res.send("IMG2DB ERR " + skc);
          });
        })

        .catch((err) => {
          console.log("Sharp Error " + err);
          res.send("ERR " + err);
        });
    }
  });
});
//---Copy Pim Product from Upfront PIM  to CLoud PIM
router.get("/PIM/:skcode", function (req, res) {
  var skc = req.params.skcode;

  qry = "select * from twdpim.product where skCode =" + skc;
  mydb.myGetQuery(qry, function (resp) {
    //console.log(resp.dbfields);

    if (resp.dbcode == 0) {
      //---------------Insert To CLD PIM
      updatePim(resp, function (isok, ret) {
        res.status(200).send("Update Result " + ret);
      });
    } else {
      res.status(200).send("Cant read from UpfrontPIM " + resp.dbmessage);
    }
  });
});
function insUpdPim(rset) {
  // 1 record set here
  try {
    var item = rset.dbitems[0];
    var qry = "Insert twdpim.produce(";
    var vstr = "values(";
    for (key in item) {
      qry += key + ",";
      var val = item[key];
      val = val.replace(/'/g, "'");
      vstr += "'" + item[key] + "',";
    }
    qry =
      qry.substring(0, qry.lastIndexOf(",") - 1) +
      ") " +
      vstr.substring(0, vstr.lastIndexOf(",") - 1);
    console.log(qry);
  } catch (ex) {
    console.log(ex.message);
  }
}

function updatePim(rset, callback) {
  // 1 record set here

  try {
    //  console.log(rset.dbfields[0]);

    var item = rset.dbitems[0];
    var qry = "update twdpim.product  set ";
    var prcode = item.prCode;
    for (key in item) {
      if (key != "prCode" && key != "skCode") {
        var val = item[key];

        if (val == null) val = ""; //if it is null dont update
        //--- if it is date time then
        var cty = rset.dbfields[0][key]; //...diff struct from mssql
        if (cty == 12 && val.indexOf("-") < 0) val = "2019-01-01"; //--value not date then assign date

        //	console.log(key + ' ' +cty +' val '+val);
        //---------------------------
        var regx = RegExp("'", "g");
        if (typeof val === "string") {
          val = val.replace(regx, "'");
        }

        qry += key + "= '" + val + "' ,";
      }
    }
    qry =
      qry.substring(0, qry.lastIndexOf(",") - 1) +
      " where prCode ='" +
      prcode +
      "'";

    //	  console.log(qry);
    mydb.execMysql(qry, function (ret) {
      console.log(ret);
      callback(ret.dbcode == 0, ret.dbmessage);
    });
  } catch (ex) {
    console.log(ex.message);
    callback(false, ex.message);
  }
}

//---Copy Pim Product from Upfront PIM  to CLoudPOS
router.get("/PIM2POS/:skcode", function (req, res) {
  var skc = req.params.skcode;

  qry = "use.UBUPIM^select * from twdpim.posprodview where SK_CODE =" + skc; //view that structure according to POSPROD
  db.dbGetQuery(qry, function (resp) {
    //console.log(resp.dbfields);

    if (resp.dbcode == 0) {
      //---------------Insert To CLD PIM
      insertPOS(resp, function (isok, ret) {
        res.status(200).send("Insert POSPROD " + ret);
      });
    } else {
      res.status(200).send("Cant read from UpfrontPIM " + resp.dbmessage);
    }
  });
});

function insertPOS(rset, callback) {
  // 1 record set here
  try {
    var item = rset.dbitems[0];
    var skc = item.SK_CODE;
    var qry =
      "if not exists(Select * from POSPROD Where Sk_CODE=" +
      skc +
      ") Insert CLOUDPOS(ST_CODE,";

    var vstr = "values('920',";

    //	console.log(item);
    for (key in item) {
      qry += key + ",";
      var val = item[key];
      //console.log(val);
      if (val == null) val = ""; //if it is null dont update
      //--- if it is date time then
      var cty = rset.dbfields[0][key]; //...diff struct from mssql
      if (cty == 12 && val.indexOf("-") < 0) val = "2019-01-01"; //--value not date then assign date
      if (typeof val == "string") val = val.replace(/'/g, "'"); //replace ['] with [\']

      vstr += "'" + item[key] + "',";
    }
    qry =
      qry.substring(0, qry.lastIndexOf(",") - 1) +
      ") " +
      vstr.substring(0, vstr.lastIndexOf(",") - 1);
    //console.log(qry);
    db.execute(qry, function (ret) {
      callback(ret.dbcode == 0, ret.dbmessage);
    });
  } catch (ex) {
    console.log(ex.message);
    callback(false, ex.message);
  }
}

router.get("/UPDEPROD", function (req, res) {
  var query = require("url").parse(req.url, true).query;
  var itms = { name: query.name, brand: query.brand, skcode: query.skcode, enrich: query.enrich, showonweb: query.showonweb };
  updEprod(itms, function (isok, ret) {
    res.status(200).send(ret);
  });
});

function updEprod(itms, callback) {
  try {
    var qry =
      "update CLOUDPOS..EProd set PR_NAME = LEFT('@name',100) ,PR_BRAND = LEFT('@brand',20)    where SK_CODE =@skcode;";
    qry +=
      " update TWDPIM..product set prNameTH = LEFT('@name',100),brand = LEFT('@brand',50),enrich='@enrich', showOnWeb='@showonweb'    where SKCODE =@skcode;";
    var skc = itms.skcode;
    var pdata = {
      qry: qry,
      par: { skcode: itms.skcode, name: itms.name.replace(/'/g,"''"), brand: itms.brand.replace(/'/g,"''"), enrich: itms.enrich, showonweb: itms.showonweb },
    };
    console.log(pdata)
    db.execute(pdata, function (resp) {
      //--read from UBUTUPIM
      if (resp.dbcode == 0) {
        callback(true, "Copy Pim Name  " + skc + "  Result " + resp.dbmessage);
      } else {
        callback(false, "Cant read Update Eprod " + resp.dbmessage);
      }
    });
  } catch (ex) {
    callback(false, ex.message);
  }
}
module.exports = router;
