
module.exports = (err, req, res, next) => {
    const statusCode = err.statusCode || 500;
    const pageurl = err.pageurl ;
    if(pageurl){
        return res.redirect(`${err.pageurl}?message=${err.message}`);
        // return res.render(`./pages/${page}.ejs`,{ title:'PDA Login' ,message:err.message});
    }else{
        return res.status(statusCode).json({
            error: {
                status_code: statusCode,
                message: err.message,
                validation: err.validation
            }
        });
     }

}