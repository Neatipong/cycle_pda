var express = require('express');
var router = express.Router();
const pageController = require('../controllers/pageController');
const passportJWT = require('../middleware/passportJWT');

// router.get('/' , [ passportJWT.isLogin ] ,pageController.index);
// router.route("/" )
//     .get([ passportJWT.isLogin ],pageController.index) 
//     .post()
//     .delete()
router.route("/" )
    .get(pageController.index)
    .post()
    .delete()

router.route("/changepassword",)
    .get(pageController.changepassword)

router.route("/maincontrol")
    .get([passportJWT.isLogin],pageController.maincontrol)

router.route("/scanonly")
    .get([passportJWT.isLogin],pageController.scanonly)

router.route("/scannumber")
    .get([passportJWT.isLogin],pageController.scannumber)

router.route("/scannewproduct")
    .get([passportJWT.isLogin],pageController.scannewproduct)

router.route("/scanwithnumber")
    .get([passportJWT.isLogin],pageController.scanwithnumber)

router.route("/editvariance")
    .get([passportJWT.isLogin],pageController.editvariance)

router.route("/selectstore")
    .get([passportJWT.isLogin],pageController.selectstore)

router.route("/edititemcount")
    .get([passportJWT.isLogin],pageController.edititemcount)

router.route("/listitemnotcount")
    .get([passportJWT.isLogin],pageController.listitemnotcount)
module.exports = router;