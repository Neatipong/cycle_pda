var express 	= require('express')
 
var router 		= express.Router()
var dt 			= require('../resource/datetime') 
var request		=require('request')
var db			=require('../resource/dbconnect') 
const sharp = require('sharp');
var fs 		=require('fs')
// https://sharp.pixelplumbing.com/en/stable/api-input/

router.use(function timeLog (req, res, next) {
	  console.log('Call Image Router.   Time: ', dt.myDateTime())
	  next()
	})
	
router.post('/URLTODB' , function (req, res) {
    
	var imgurl  =	req.body.url;
    var cat		= 	req.body.cat;
    var ucat		= req.body.ucat;
	cat=(cat+'00000000').slice(0,6);
	console.log('Save url  ' + imgurl +' to DB '+cat)

	request({url: imgurl, encoding: null}, 
			function(err, result, bodyBuffer) {
				if(err){
					console.log('Image2DB .'+err);
					res.end('Image2DB ERR Cant get Image from '+imgurl)
				}
				else {
					sharp.cache(false);
					sharp(bodyBuffer)
					.resize(630,340)  //.resize(500,500)
			    	.toBuffer()
			    	.then( async data=>{
                        var buf =data.toString('base64');				
                        var cmd=" exec  TWDPIM..UserCatImageUpdate '@ucat','@cat', '@base' "
                        var pdata={
                            qry:cmd,
                            par:{  cat:cat,
                                    ucat:ucat,
                                    base:buf}
                        } 
                         
                        db.execute(pdata,function(resp){  //-----return with req ID
                            if(resp.dbcode==0){
                                var msg  = resp.dbmessage;
                                //if(resp.dbrows>0)msg="Cannot Update Image"
        
                                console.log('Copy & Resize Image to USRCAT ' +ucat + cat +' '+msg);
                            
                                
                                res.status(200).end('Copy & Resize Image to USRCAT ' +ucat + cat +' '+msg);
                            }
                            else {
                                console.log('Save image Error ' +resp.dbmessage);
                                res.status(200).end('ERR '+ resp.dbmessage);
                            }
                                        
                        });
                        buf=null;
                                
                    }) 
			    	.catch( err => {
			    		     console.log('Sharp Error '+err);
			    		     res.send('image2DB ERR '+err)
			    			} 
			    	      );
					
				}

		});

});
 
router.post('/FILETODB' , function (req, res) {
 
	//var fname  =req.body.fname; 
	var fname  ='./public/uploads/userPhoto.jpg'; // req.body.fname;
	
    var cat = req.body.cat;
    var ucat = req.body.ucat;
	cat=(cat+'00000000').slice(0,6);

	console.log('Convert file '+fname +' to DB '+cat);
	
	sharp.cache(false);
	if (ucat == '28LV') {
		sharp(fname)		
		.resize(630,340)  //.resize(500,500)
		.toBuffer() 
		.then( async data=>{
				var buf =data.toString('base64');				
                var cmd=" exec  TWDPIM..UserCatImageUpdate '@ucat','@cat', '@base' "
				var pdata={
                    qry:cmd,
                    par:{  cat:cat,
                            ucat:ucat,
                            base:buf}
                } 
				 
				db.execute(pdata,function(resp){  //-----return with req ID
					if(resp.dbcode==0){
						var msg  = resp.dbmessage;
					    //if(resp.dbrows>0)msg="Cannot Update Image"

						console.log('Copy & Resize Image to USRCAT ' +ucat + cat +' '+msg);
					
						
						res.status(200).end('Copy & Resize Image to USRCAT ' +ucat + cat +' '+msg);
					}
					else {
						console.log('Save image Error ' +resp.dbmessage);
						res.status(200).end('ERR '+ resp.dbmessage);
					}
								
				});
				buf=null;
						
			})
					
		.catch( err => {
					console.log('Sharp Error '+err);
					res.send('ERR image2DB  '+err);
				} 
				);
	}
	else {
		sharp(fname)		
		//.resize(630,340)  //.resize(500,500)
		.toBuffer() 
		.then( async data=>{
				var buf =data.toString('base64');				
                var cmd=" exec  TWDPIM..UserCatImageUpdate '@ucat','@cat', '@base' "
				var pdata={
                    qry:cmd,
                    par:{  cat:cat,
                            ucat:ucat,
                            base:buf}
                } 
				 
				db.execute(pdata,function(resp){  //-----return with req ID
					if(resp.dbcode==0){
						var msg  = resp.dbmessage;
					    //if(resp.dbrows>0)msg="Cannot Update Image"

						console.log('Copy & Resize Image to USRCAT ' +ucat + cat +' '+msg);
					
						
						res.status(200).end('Copy & Resize Image to USRCAT ' +ucat + cat +' '+msg);
					}
					else {
						console.log('Save image Error ' +resp.dbmessage);
						res.status(200).end('ERR '+ resp.dbmessage);
					}
								
				});
				buf=null;
						
			})
					
		.catch( err => {
					console.log('Sharp Error '+err);
					res.send('ERR image2DB  '+err);
				} 
				);
	}
});

router.post('/BASE64TODB' , function (req, res) {
	var imageData  	= req.body.base64;  //'./public/uploads/userPhoto.jpg'; // req.body.fname;
    var cat 		= req.body.cat;
    var ucat 		= req.body.ucat;
	cat=(cat+'00000000').slice(0,6);
	console.log('Convert Base64  to DB '+cat);

	var img = new Buffer(imageData, 'base64');
	sharp.cache(false);
    sharp(img)	
	.resize(630,340)  //.resize(500,500)
	.toBuffer()
	.then( data=>{
		var buf =data.toString('base64');
        var cmd=" exec  TWDPIM..UserCatImageUpdate '@ucat','@cat', '@base' "
        var pdata={
            qry:cmd,
            par:{  cat:cat,
                    ucat:ucat,
                    base:buf}
        } 

		db.execute(pdata,function(resp){  //-----return with req ID

					if(resp.dbcode==0){
						console.log('Save base 64 image OK')
						res.status(200).end('base64db Copy & Resize Image Successful')
					}
					else {
						console.log('Base64image Error ' +resp.dbmessage)
						res.status(200).end('Base 64 ERR '+resp.dbmessage);
					}
						
				});
			}) 
	.catch( err => {
					console.log('Sharp Error '+err);
					res.status(200).send('image2DB ERR '+err)
				} 
		 );		
	 
});
 
 
module.exports = router