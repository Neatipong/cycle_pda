const 	config = require('config');
var  myConfig = config.get('mysqlConfig');
var mysql = require('mysql');

exports.myGetQuery = function(sql,callback){
//	var posconfig =config.get('posmenu');
//	var defaultdb =posconfig.get('defaultdb');
	 myConfig = config.get('mysqlConfig');
	 myGetQuery2(sql,function(ret){
		    callback(ret);
	 })
}  

//-----------get PIM from upfront PIM on ubutu server
exports.ubuGetQuery = function(sql,callback){
	 myConfig = config.get('myubuConfig');
	 myGetQuery2(sql,function(ret){
		    callback(ret);
	 })
} 

//--------excute on Cloud PIM  with para ----
exports.execute =function(req,callBack){
	var xid =req.par.xid;
	
	if ((xid===null)|| (typeOf(xid)==='undefined') ){
		var resp={
				xid:null,
				dbcode:999,
				dbmessage:"No Paraemter Specify",
				dbitems:null
		  }
		callBack(0,resp)
	}
	var para=req.par;
	var cmd =req.qry;
	//-------Replace cmd with parameter
	for(key in para){
		var rp =RegExp('@'+key,'g');
		cmd=cmd.replace(rp,para[key]);
	}
	//-------exucute
	 myConfig = config.get('mysqlConfig');
	myGetQuery2(cmd,function(resp){
		resp.xid=xid;
		console.log('exe ret  '+resp);
		callBack(resp);
	});
	 
}

//--------excute on Cloud PIM  with para ----
exports.execMysql =function(qry,callback){
	 var resp ={
			 dbcode:99,
			 dbrows:0,
			 dbmessage:"ERROR"
   			}
	//-------execute
	 myConfig = config.get('mysqlConfig');

	 var con = mysql.createConnection({
		  host:myConfig.host,  // "ubu.upfront.co.th",
		  user: myConfig.userid,
		  dateStrings: true,
		  password:myConfig.password // "upfront"
		 });

		con.connect( function(err) {
		  if (err){ 
		   console.log("Conn MYSQL "+err);
		   resp.dbmessage=err;
		   callback(resp);
		   } 
		  else { 
		     con.query(qry, function (err, results,flds) {
				if (err){
					console.log(' Exec Mysql '+err);
					 resp.dbmessage=err;
					 callback(resp);
				}
				else {
					 resp.dbcode=0;
					 resp.dbrows=results.affectedRows;
					 resp.dbmessage="OK";
					 callback(resp);
					}
				});
		  } 
		});
}
 
var http=require('http')
const querystring = require('querystring');

function myGetQuery2(sql,callback){
  try {     
		var con = mysql.createConnection({
		  host:myConfig.host,  // "ubu.upfront.co.th",
		  user: myConfig.userid,
		  dateStrings: true,
		  password:myConfig.password // "upfront"
		});

		con.connect(function(err) {
		  if (err)throw(err);
		  console.log("Query MYSQL "+sql);
		});
		  
		var ecode =0;
		var emsg="OK";
		var fields =[];
		    
	   con.query(sql, function (err, results,flds) {
			if (err){
				//---return error
			//	console.log("Connect Error .."+err);
				ecode	=err.errno;
				emsg	=err.sqlMessage;
				 var resp ={
						 dbcode:ecode,
						 dbmessage:emsg
			   			}
		   	    callback(resp);
				}
			
			else { 
 			    
					var item = {}; 
					for(var i=0;i<flds.length;i++){ 
						var cnm 	=flds[i].name;
						item[cnm] 	=flds[i].type;
						}  
					fields.push(item); 
			   	  
		     var resp ={
	 				 dbcode:ecode,
	 				 dbmessage:emsg,
	 				 dbfields:fields,
	 				 dbrows:results.length,
	 				 dbitems:results
	 	   			}
		     console.log('Mysql return rows '+results.length)		;     
	    	  callback(resp);
			}
		});
	  con.end(function(err){
	     if(err){ 
			console.log("Con end with Error.."+ err);
			//ecode	=err.errno;
			 emsg	="Error"; //err.sqlMessage;
			 var resp ={
					 dbcode:99,
					 dbmessage:emsg
		   			}
		     
	   	   callback(resp);
	   }
	  });
  }
  catch(ex){
	  console.log('MySql Get query Err'+ex.message);
	  var resp ={
				 dbcode:998,
				 dbmessage:ex.message
	   			}
	     
	   callback(resp);
       }
	}

 