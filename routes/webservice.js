var express = require('express');
var router 	= express.Router();
var request  =require('request')
/* GET product service */
var db		=require('../resource/dbconnect')
//var cld		=require('../resource/cldconnect')
var dt		=require('../resource/datetime')

router.get('/VERCUST/:eml/:passw', function (req, res, next) {	 
    var custid=req.params.custid;
    var email=req.params.eml;
    var passw=req.params.passw;
    console.log('verify Customer ..'+custid);
    var resp={dbcode:99,
            message:'invalid parameter'}

	if(custid===null){
		res.json(resp);
		return;
	}
 
	var qry="exec  verifyCust  '" + email+ "','" + passw +"'";
     console.log(qry)
     
	 db.dbGetQuery(qry,function(resp){
         res.send(resp);
          
     });  //--end of first query
     
    });

router.get('/CUSTINFO/:custid', function (req, res, next) {	 
        var custid=req.params.custid;
 
        console.log('TWD Customer ..'+custid);
        var resp={dbcode:99,
                message:'invalid parameter No Custid Specified'}
    
        if(custid===null){
            res.json(resp);
            return;
        }
     
        var qry="exec getCustInfo'"+ custid +"' ";
         
         db.dbGetQuery(qry,function(resp){
             res.send(resp);
              
         });  //--end of first query
         
        });

     

     router.get('/PORDER/:ref', function (req, res, next) {	 
            var ref=req.params.ref;
     
            console.log('Order  ..'+ref);
            var resp={dbcode:99,
                    message:'invalid parameter No ref Specified'}
        
            if(ref===null){
                res.json(resp);
                return;
            }
         
            var qry="exec getPorder '"+ ref +"' ";
             
             db.dbGetQuery(qry,function(resp){
                 res.send(resp);
                  
             });  //--end of first query
             
            });
   
router.post('/QUERY' , function (req, res) {
        console.log('call POSSRV/QUERY');
        if(!req.body.query){
            res.status(200).json({
                    dbcode:999,
                    dbmessage:'query is not defined'  ,
                    dbitems:null 
                   })
            return; 
        }
        else { 
         var qry=req.body.query;
         var par=req.body.para;
         //--replace parameter if any
         if(par!=null){
             for(key in par){
                 var rp =RegExp('@'+key,'g');
                 qry=qry.replace(rp,par[key]);
             }
          }
          db.dbGetQuery(qry,function(resp){
               // 	console.log(resp);
                 res.status(200).json(resp);
             });
        }
    });

    router.post('/CONFIRMPAYMENT' , function (req, res) {
        console.log('call POSSRV/QUERY');
        if(!req.body.payment){
            res.status(200).json({
                dbcode:999,
                dbmessage:'payment is not defined'  ,
                dbitems:null 
               })
        return; 
 
            return; 
        }
        else { 
          var paym =req.body.payment;
          var ref  =req.body.reference
          var dlv  =req.body.deliver
          var qry=" exec confirmpayment '@ref','@payment','@deliver'";
          qry=qry.replace('@payment',JSON.stringify(paym))
          qry=qry.replace('@deliver',JSON.stringify(dlv))

          qry=qry.replace('@ref',ref)
          console.log(qry)

          db.dbGetQuery(qry,function(resp){
              	console.log(resp);
                  res.status(200).json(resp)
             });
        }
    });
  
// {sendto:xxx,subject:xxx,mailbody:htmlbody}
var nodemailer = require('nodemailer'); 
router.post('/EMAIL' , function (req, res) {
	console.log(req.body);
	//return;
	if(!req.body.sendto){
		 res.send('No Receipient');
		 res.end();
	   }
	else {
		
		//---dont forget to turn less secure on GOOGLE MAIL
		//-- https://myaccount.google.com/u/1/lesssecureapps?pageId=none
		//GMAIL block send message>>except sert less secure above
	   
		 var transporter = nodemailer.createTransport({
			  service: 'gmail', //proble with Gamil
			  auth: {
			    user: 'upfposbynode@gmail.com',
			    pass: 'Upfront20'
			  }
			});
	/*
			//use admins@upfront.co.th paasw Upf@PAhol4	
		   // mail server smtp.upfront.co.th
		var smtpTransport = require("nodemailer-smtp-transport");
		 var transporter  =nodemailer.createTransport(smtpTransport(
				 {
				  host: 'smtp.upfront.co.th/webmail',
				  port:25,
				  secure: false, // upgrade later with STARTTLS
				  auth: {
				    user: "admin@upfront.co.th",
				    pass: "Upf@PAhol4"
				  }
				 }));
 
		//--------*/
			var mailOptions = {
			  from: 'Upfront application Admins',
			  	to:  req.body.sendto,
			  subject:  req.body.subject,
			  html:  req.body.mailbody
			};
			
			transporter.sendMail(mailOptions, function(error, info){
			  if (error) {
			     console.log(error);
			    res.end('Error send mail ..'+error);
			  } else {
			    console.log('Email sent: ' + info.response);
			   res.end('Email has been sent to : ' + req.body.sendto);
			  }
			});
	}
     
});    

router.get('/ZIPCODE/:zcode', function (req, res, next) {	 
  var zip=req.params.zcode;

  console.log('Find Zipcode  ..'+zip);
  var resp={dbcode:99,
          message:'invalid parameter No zipcodeSpecified'}

  if(zip===null){
      res.json(resp);
      return;
  }

  var qry="Select * from postalcode where zipcode = '"+ zip +"' ";
   
   db.dbGetQuery(qry,function(resp){
      //console.log(resp);
       res.send(resp);
      
   });  //--end of first query
   
  });

module.exports = router;