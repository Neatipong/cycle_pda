'use strict';
 
const express = require('express')


var http = require('http');  //.createServer(app);
var https = require('https');
const bodyParser = require('body-parser')
require('body-parser-xml')(bodyParser); 
var 	dt 		=require('./resource/datetime');
var  dbcnn 		=require('./resource/dbconnect');
const fs		=require('fs'); 
const cookieParser = require('cookie-parser');
const config	=require('config');
global.atob 	=require("atob");
var request		=require('request');

const passport = require('passport');
const prdsrv	=require('./routes/posservice');
var path = require('path');

const nodemailer = require('nodemailer');
const axios = require('axios');
const setPublic = path.join(__dirname, 'public');
const errorHandler = require('./middleware/errorHandler');
var app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('routes', path.join(__dirname, 'routes'));


app.set('view engine','ejs');
const mnConfig = config.get('posmenu');
var  port 		= mnConfig.port ; 
global.inetgateway =mnConfig.inetgateway ; 

const { json } = require('body-parser');

var options = {
			root: setPublic,
			dotfiles: 'deny',
			headers: {
				'x-timestamp': Date.now(),
				'x-sent': true
			}
		  }; 
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');  //allow connection from any location!!! Importance
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

//app.use(express.json()); 
//app.use(express.urlencoded());
//------solve problem ... PayloadTooLargeError: request entity too large

app.use(cookieParser());
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }))
app.use(express.static('public'))   //--allow static page to be display
app.use(bodyParser.xml({
	limit: '50mb',   // Reject payload bigger than 50 MB
	xmlParseOptions: {
	  normalize: true,     // Trim whitespace inside text nodes
	  normalizeTags: false, // Transform tags to lowercase
	  explicitArray: false // Only put nodes in array if >1
	}
  }));
app.use(passport.initialize());


 
// app.get('/', (req, res) => res.sendFile("maincontrol.html", options, function (err) {
// 	if (err) {
// 	  res.send(err);
// 	} else {
// 	  console.log('Calling control');
// 	}
//   }) 
// )
app.use('/POSSRV',prdsrv)
app.use('/test', require('./routes/testRoute.js'));

app.use('/api', require('./routes/apiRoute.js'));
app.use('/', require('./routes/pageRoute.js'));

app.use(errorHandler);
//app.use('/users',users);

// app.get('/', (req, res) => res.render('pages/login.ejs',{ title:'PDA Login'}));


// var httpsOptions = {
//     key:  fs.readFileSync('CERT/upfdev_com.key'),
//     cert: fs.readFileSync('CERT/upfdev_com.crt')
// };
 

http.createServer(app).listen(85,
	function(){
		  console.log(dt.myDateTime() +' listening on *:85');
	    });
	 
// https.createServer(httpsOptions, app).listen(433,
// 	function(){
// 		console.log('https listening on *:433');
// 	  });