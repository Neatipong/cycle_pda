function pageReady(){
	if (profileList.indexOf('0030') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู User');
		location.assign("/");
	}

	  $('#showDiv').mouseleave(function () {
		   //     $('#detailCard').hide();
		        $('#popUpMenu').hide();
		    });
}
var actionLine = 0;
var editObj;
function menuOver(obj) {
	editObj =obj;
    var pos = $(obj).position();
  //  var mpos = $('#mainDiv').position();
    actionLine = $(obj).find('td:eq(0)').text();
    $('#popUpMenu').css('top', pos.top);
	var left=$(window).width();
	$('#popUpMenu').css('left', cursorX);
    $('#popUpMenu').show();
   
}
function editItem(rec ){
	//alert('Call for detail');
	$('#popUpMenu').hide();
	try{
		var card=0;
		var pos={top:100,left:0};
		if(editObj){
			  card=$(editObj).find('td:eq(0)').text(); 
			   pos =$(editObj).position();
		}
		
		
		if(rec==='NEW')card='NEW';
	    var pdata={
	    		patname:'./SYSTEM/edituser.txt',
	    		para:{
	    			stc:storeCode,
	    			rec:card
	    			}
	    	}
	    ajaxXpandFile(pdata,function(isok,data){
	    	console.log("AJAX call Edit "+isok);
			$('#editModal').html(data);
			$('#editModal').modal('show')  ;
	    })

	 
	}
	catch(ex){alert(ex.message);}
}

function deleteItem(){
	try{
		var rcpt =$(editObj).find('td:eq(0)').text();
		if(confirm('Remove this user'+rcpt)==0)return;
		//alert('VOid');
		var cmd="delete USERTAB where recid=  @rec";
		var pdata={
				query:cmd,
				para:{stcode:storeCode, 
					   rec:rcpt} 
		  }
		ajaxExec(pdata,function(isok,resp){
			if(isok){
				var jsn =JSON.parse(resp);
				if(jsn.dbcode==0){
//alert('User has been removed');
					   //--reload this page
					   $('#editModal').modal('hide')  ;
					   pageReload();
				}
				else
					alert(jsn.dbmessage);
			}
			else
				alert('System error Cant call service');
		});
	}
	catch(ex){alert('delete function Error..'+ex.message);}
}

function hideDetail(){
	$('#editModal').modal('hide')  ;
}
function confirmEntUser(){
	try{

		var usid 	=$('#usrtxt').val();
		usid=usid.trim();
		if(usid.length == 0){
			alert('กรุณาใส่ User ID');
			$('#usrtxt').focus();
			return
		}

		var fnm 	=$('#unmtxt').val();
		fnm=fnm.trim();
		if(fnm.length == 0){
			alert('กรุณาใส่ Full Name');
			$('#unmtxt').focus();
			return
		}

		var grp 	=$('#grptxt').val();
		grp=grp.trim();
		if(grp.length == 0){
			alert('กรุณาใส่ Group');
			$('#grptxt').focus();
			return
		}


		var email 	=$('#emltxt').val();
		email=email.trim();
		if(email.length == 0){
			alert('กรุณาใส่อีเมล');
			$('#emltxt').focus();
			return
		}

		if(email.indexOf('@')<0){
			alert('อีเมลไม่ถูกต้อง');
			$('#emltxt').focus();
			return
		}

		//////////////////////////
		var rec = $('#rectxt').val();	
		
		var cmd="Update USERTAB set USERID='@usr',FULLNAME='@unm',EMAIL='@eml',USERGROUP='@grp',STCODE='@stc' Where RECID= @rec ";
		if(rec=='NEW')
			cmd="Insert USERTAB(USERID, FULLNAME,USERGROUP,STCODE,EMAIL,PASSWORD)  " 
				+"values('@usr','@unm','@grp','@stc','@eml',dbo.encrypt('@usr','NEWPASSW') )";
		
		var pdata={
				query:cmd,
				para:{ 
				      usr:$('#usrtxt').val(),
				      unm:$('#unmtxt').val(),
				      grp:$('#grptxt').val(),
					  eml:$('#emltxt').val(),
					  stc:$('#stctxt').val(),
				      rec:rec
				      }
		  }
		ajaxExec(pdata,function(isok,resp){
			if(isok){
				var jsn =JSON.parse(resp);
				if(jsn.dbcode==0){
					  // alert('User Tab  has been changed');
					   $('#editModal').modal('hide')  ;
					   //--reload this page
					   pageReload();  //--load same Pattern again
				}
				else
					alert(jsn.dbmessage);
			}
			else
				alert('System error Cant call service');
		});
	}
	catch(ex){alert('delete function Error..'+ex.message);}
}
function cancelEntUser(){
	$('#editModal').modal('hide')  ;
}
//---Add New item
function addNewItem(){
	//alert('Add New');
	editItem('NEW');
}

/*** 
$HDR
<div id ='showDiv' class='container'>
<div class='row'>
<h2>USER CARDID Control</h2>
<a href ='javascript:addNewItem()' class='btn btn-info pull-right'>Add New</a>

<table class='table'>
<thead><th>Rec<th>USERID<th>NAME<th>GROUP<th>Email<th>LAst Login<th>OPR Store</thead>
$SQL
Select * from USERTAB
$BDY
<tr onmouseover ='menuOver(this)' >
<td>^RECID^<td>^USERID^<td>^FULLNAME^<td>^USERGROUP^<Td>^EMAIL^<td>^LASTLOGIN^<td>^STCODE^</tr>
$ERX
<tr><td colspan='6'>Error @error
$END
</table>
  
  <div id='popUpMenu' style='display:none;z-index:6;position:absolute;'>
     <a href="javascript:deleteItem()" class="btn smn btn-danger">Delete</a>
     <a href="javascript:editItem()" class="btn smn btn-primary">Edit</a>
</div>
</div>
</div>
$xxx

//END  **/